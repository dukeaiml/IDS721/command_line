# Rust Command-Line Tool with Testing for Data Processing

This project leverages the clap library to develop a command-line utility designed for CSV data manipulation. The utility will consume data from a CSV file and output the processed data according to the command-line arguments specified by the user. In this project, we use the tooth growth dataset and we use the command-line tool to input our data processing and filtering requirement. Meanwhile, we contained a unit testing and allow the generation of test reports after testing.

## Goals
* Rust command-line tool
* Data ingestion/processing
* Unit tests

## Steps
### Step 1: Initialize Rust Project
1. Create a new Rust project. Use the `--bin` to produce the binary executable for the command line tool later.
```
cargo new <PROJECT_NAME> --bin
```
2. Add necessary dependencies to `Cargo.toml` file. This project uses `clap` to create a command-line tool for csv processing.
```
csv = "1.1.6"
clap = "2.27.1"
serde = { version = "1.0", features = ["derive"] }
serde_json = "1.0.68"
tokio = { version = "1", features = ["full"] }
anyhow = "1.0.0"
```
3. Prepare the `.csv` file dataset under the project directory for processing. Below is a part of the whole dataset for preview:
```
len,supp,dose
4.2,VC,0.5
11.5,VC,0.5
7.3,VC,0.5
5.8,VC,0.5
6.4,VC,0.5
10,VC,0.5
...
```

### Step 2: Main Command-line Tool Function
1. Write the Command Line tool function and the data processing in the `main.rs`.

### Step 3: Testing Implementation
1. Include a unit test to ensure the correctness and reliability of the tool's functionalities.

## Functionality, Testing, and Results
This tool accepts four arguments: `file`, `supplement`, `low`, and `high`:

* `file`: Path of the CSV file
* `supplement`: the supplement type for tooth growth
* `low`: Lower bound of the dose
* `high`: Upper bound of the dose

### Example Usage
```
cargo run tooth_length.csv --supplement 'VC' --low 4 --high 10
```
or for short and easier implementation
```
cargo run tooth_length.csv -s 'VC' -l 4 -h 10
```
### Results
![command1](https://gitlab.com/dukeaiml/IDS721/command_line/-/wikis/uploads/991657a02903d74d523d212d9b24d573/command1.png)

### Testing and Testing Report
* Test the command-line tool implementation using `cargo test`.
![test](https://gitlab.com/dukeaiml/IDS721/command_line/-/wikis/uploads/853da17b871ddd671123a63aabcffc6e/report.png)






