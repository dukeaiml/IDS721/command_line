use anyhow::Result;
use clap::{App, Arg};
use serde::Deserialize;
use tokio;

#[derive(Debug, Deserialize)]
struct Record {
    len: f64,
    supp: String
}

#[tokio::main]
async fn main() -> Result<()> {
    let matches = App::new("CSV Filter Tool")
        .version("1.0.0")
        .author("Your Name")
        .about("Filters records in a CSV file based on supp type and len range")
        .arg(Arg::with_name("file")
             .help("Input CSV file path")
             .required(true)
             .index(1))
        .arg(Arg::with_name("supp")
             .help("Supplement type to filter by")
             .short("s")
             .long("supplement")
             .takes_value(true)
             .required(true))
        .arg(Arg::with_name("low")
             .help("Lower bound of len")
             .short("l")
             .long("low")
             .takes_value(true)
             .required(true))
        .arg(Arg::with_name("high")
             .help("Upper bound of len")
             .short("h")
             .long("high")
             .takes_value(true)
             .required(true))
        .get_matches();

    let file_path = matches.value_of("file").unwrap();
    let supp_filter = matches.value_of("supp").unwrap();
    let low_len: f64 = matches.value_of("low").unwrap().parse()?;
    let high_len: f64 = matches.value_of("high").unwrap().parse()?;

    let mut rdr = csv::Reader::from_path(file_path)?;
    for result in rdr.deserialize() {
        let record: Record = result?;
        if record.supp == supp_filter && record.len >= low_len && record.len <= high_len {
            println!("{:?}", record);
        }
    }

    Ok(())
}

mod tests {
    use super::*;

    #[tokio::test]
    async fn test_filter_oj_between_10_and_20() -> Result<(), Box<dyn std::error::Error>> {
        let path = "tooth_length.csv";
        let supplement = "OJ";
        let low: f64 = 10.0;
        let high: f64 = 20.0;

        let mut rdr = csv::Reader::from_path(path)?;
        let mut filtered_records = Vec::new();

        for result in rdr.deserialize() {
            let record: Record = result?;
            if record.supp == supplement && record.len >= low && record.len <= high {
                filtered_records.push(record);
            }
        }

        // Example assertions
        assert!(!filtered_records.is_empty(), "Should have at least one record");
        for record in filtered_records {
            assert_eq!(record.supp, "OJ", "Supplement should be OJ");
            assert!(record.len >= 10.0 && record.len <= 20.0, "Length should be between 10 and 20");
        }

        Ok(())
    }
}


